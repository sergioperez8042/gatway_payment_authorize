
from rest_framework import routers

from . import views
from django.urls import include, path


routes = routers.DefaultRouter()
routes.register(r'transactions', views.TransactionViewSet)
routes.register('merchant', views.MerchantViewSet)


urlpatterns = [
    path('', include(routes.urls)),

]
