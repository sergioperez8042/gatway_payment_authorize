import datetime
from django.http import HttpResponse

import requests
from api.models import TransactionStatus
from api.credit_card import CreditCard

from api.models import Transaction, TransactionStatusType


def charge_behavior(transaction: Transaction, credit_card: CreditCard, created_status: TransactionStatusType):
    url = 'https://api.authorize.net/xml/v1/request.api'
    data = {
        'createTransactionRequest': {
            'merchantAuthentication': {
                'name': transaction.merchant_account.username,
                'transactionKey': transaction.merchant_account.password
            },
            'refId': str(transaction.id),
            'transactionRequest': {
                'transactionType': 'authCaptureTransaction',
                'amount': str(round(transaction.amount, 2)),
                'payment': {
                    'creditCard': {
                        'cardNumber': credit_card.card_number,
                        'expirationDate': datetime.datetime.strptime(credit_card.card_expiry, '%m%y').strftime("%Y-%m"),
                    }
                },
                "order": {
                    "invoiceNumber": transaction.id
                }
            }
        }
    }

    response = requests.post(
        url=url,
        json=data
    )
    if 200 > response.status_code or response.status_code > 299:
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    response.encoding = 'utf-8-sig'
    response_json: dict = response.json()

    if 'refId' not in response_json.keys():
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' +
            'No reference id in response: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    ref_id = response_json['refId']

    if ref_id != str(transaction.id):
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(
                response.status_code) + ': ' + 'Reference id in response is incorrect: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    if 'transactionResponse' not in response_json.keys():
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(
                response.status_code) + ': ' + 'No transaction response in response: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    transaction_response = response_json['transactionResponse']

    if 'responseCode' not in transaction_response.keys():
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' +
            'No response code in response: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    resp_code = transaction_response['responseCode']

    if resp_code == '2':
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' +
            'Transaction was declined: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    if resp_code == '3':
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(
                response.status_code) + ': ' + 'There was an error during transaction processing: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    if 'transId' not in transaction_response.keys():
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' +
            'No transaction id in response: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()

    ret_ref = transaction_response['transId']
    transaction.ref_id = ret_ref
    transaction.save()

    if resp_code == '4':
        raise ConnectionRefusedError()

    if resp_code == '1':
        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Completed'),
            merchant_response=str(response.status_code) +
            ': ' + 'Transaction charged: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        return

    transaction_status = TransactionStatus(
        transaction_id=transaction.id,
        transaction_type=transaction.transaction_type,
        transaction_status_type=TransactionStatusType.objects.get(
            name='Error'),
        merchant_response=str(response.status_code) + ': ' +
        'Unknown status from merchant: ' + response.text
    )
    transaction_status.save()
    created_status.delete()
    raise ConnectionRefusedError()
