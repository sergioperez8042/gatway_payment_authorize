import datetime


import requests
from api.credit_card import CreditCard

from api.models import Transaction, TransactionLog, TransactionStatus, TransactionStatusType


def authorize_behavior(transaction: Transaction, credit_card: CreditCard, created_status: TransactionStatusType):

    url = 'https: // developer.authorize.net/'
    try:
        data = {
            'createTransactionRequest': {
                'merchantAuthentication': {
                    'name': transaction.merchant_account.username,
                    'transactionKey': transaction.merchant_account.password
                },
                'refId': str(transaction.id),
                'transactionRequest': {
                    'transactionType': 'authOnlyTransaction',
                    'amount': str(round(transaction.amount, 2)),
                    'payment': {
                        'creditCard': {
                            'cardNumber': credit_card.card_number,
                            'expirationDate': datetime.datetime.strptime(credit_card.card_expiry, '%m%y').strftime(
                                "%Y-%m"),
                        }
                    },
                    "order": {
                        "invoiceNumber": transaction.id
                    },
                    "billTo": {
                        "firstName": transaction.card_holder.card_name[:49],
                        "lastName": transaction.card_holder.card_name[50:99],
                        "company": "N/A",
                        "address": transaction.card_holder.address[:60],
                        "city": transaction.card_holder.city[:40],
                        "state": transaction.card_holder.state[:40],
                        "zip": transaction.card_holder.zip_code[:20],
                        "country": transaction.card_holder.country[:60]
                    },
                }
            }
        }

        TransactionLog.objects.create(
            transaction=transaction,
            log=str({
                    "type": "payment-request",
                    "url": url,
                    })
        )

        response = requests.post(
            url=url,
            json=data
        )

        TransactionLog.objects.create(
            transaction=transaction,
            log=str({
                    "type": "authorize-response",
                    "url": url,
                    "data": response.text
                    })
        )

        if 200 > response.status_code or response.status_code > 299:
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        response.encoding = 'utf-8-sig'
        response_json: dict = response.json()

        if 'refId' not in response_json.keys():
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'No reference id in response: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        ref_id = response_json['refId']

        if ref_id != str(transaction.id):
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'Reference id in response is incorrect: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        if 'transactionResponse' not in response_json.keys():
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                transaction_type=transaction.transaction_type,
                merchant_response=str(
                    response.status_code) + ': ' + 'No transaction response in response: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        transaction_response = response_json['transactionResponse']

        if 'responseCode' not in transaction_response.keys():
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_type=transaction.transaction_type,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'No response code in response: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        resp_code = transaction_response['responseCode']

        if resp_code == '2':
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_type=transaction.transaction_type,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'Transaction was declined: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        if resp_code == '3':
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_type=transaction.transaction_type,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'There was an error during transaction processing: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        if 'transId' not in transaction_response.keys():
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_type=transaction.transaction_type,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Error'),
                merchant_response=str(
                    response.status_code) + ': ' + 'No transaction id in response: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            raise ConnectionRefusedError()

        ret_ref = transaction_response['transId']
        transaction.ref_id = ret_ref
        transaction.save()

        if 'avsResultCode' in transaction_response.keys():
            transaction.avs_response = transaction_response['avsResultCode']
            transaction.avs_description = get_avs_description(
                transaction.avs_response)
            transaction.save()

        if resp_code == '1' or resp_code == '4':
            transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_type=transaction.transaction_type,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Completed'),
                merchant_response=str(
                    response.status_code) + ': ' + 'Transaction authorized: ' + response.text
            )
            transaction_status.save()
            created_status.delete()
            return

        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_type=transaction.transaction_type,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Error'),
            merchant_response=str(response.status_code) + ': ' +
            'Unknown status from merchant: ' + response.text
        )
        transaction_status.save()
        created_status.delete()
        raise ConnectionRefusedError()
    except Exception as ex:
        raise ex


def get_avs_description(avs_response):
    avs_dictionary = {
        'A': 'The street address matched, but the postal code did not.',
        'B': 'No address information was provided.',
        'E': 'The AVS check returned an error.',
        'G': 'The card was issued by a bank outside the U.S. and does not support AVS.',
        'N': 'Neither the street address nor postal code matched.',
        'P': 'AVS is not applicable for this transaction.',
        'R': 'Retry — AVS was unavailable or timed out.',
        'S': 'AVS is not supported by card issuer.',
        'U': 'Address information is unavailable.',
        'W': 'The US ZIP + 4 code matches, but the street address does not.',
        'X': 'Both the street address and the US ZIP + 4 code matched.',
        'Y': 'The street address and postal code matched.',
        'Z': 'The postal code matched, but the street address did not.'
    }
    return avs_dictionary.get(avs_response, 'Not AVS code found in merchant response')
