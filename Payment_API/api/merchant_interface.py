class IAbstractMerchant:
    _authorize_behavior = None
    _capture_behavior = None
    _charge_behavior = None

    def set_authorize_behavior(self, authorize_behavior):
        self._authorize_behavior = authorize_behavior

    def set_capture_behavior(self, capture_behavior):
        self._capture_behavior = capture_behavior

    def set_charge_behavior(self, charge_behavior):
        self._charge_behavior = charge_behavior

    def authorize(self):
        return self._authorize_behavior

    def capture(self):
        return self._capture_behavior

    def charge(self):
        return self._charge_behavior
