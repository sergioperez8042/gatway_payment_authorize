from rest_framework import serializers

from api.models import CardHolder


class CardHolderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardHolder
        fields = '__all__'
