
from django.utils import timezone
from rest_framework import serializers
from api.serializers.merchant_serializer import MerchantSimpleSerializer
from api.serializers.status_serializer import StatusSerializer
from api.serializers.card_holder_serializer import CardHolderSerializer

from api.models import Transaction, TransactionStatus, TransactionStatusType


class TransactionSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    application = serializers.SerializerMethodField()
    card_holder = CardHolderSerializer()
    transaction_type__name = serializers.SerializerMethodField()

    def get_status(self, obj):
        if isinstance(obj, Transaction):
            transaction_status = TransactionStatus.objects.filter(
                transaction=obj).last()
        else:
            transaction_status = TransactionStatus.objects.filter(
                transaction=obj.get("id")).last()

        if transaction_status is not None:
            serializer = StatusSerializer(instance=transaction_status)
            return serializer.data

        return {'status': 'unknown', 'date': timezone.now()}

    def get_application(self, obj):
        if isinstance(obj, Transaction):
            return obj.application.site_id
        transaction_object = Transaction.objects.get(id=obj.get("id"))
        return transaction_object.application.site_id

    def get_transaction_type__name(self, obj):
        if isinstance(obj, Transaction):
            return obj.transaction_type.name
        transaction_object = Transaction.objects.get(id=obj.get("id"))
        return transaction_object.transaction_type.name

    class Meta:
        model = Transaction
        fields = (
            'id', 'status', 'application', 'order_id', 'merchant_account',
            'amount', 'ref_id', 'ref_tag', 'date', 'card_number', 'transaction_type__name', 'card_holder',

        )

    def to_representation(self, instance):

        status = instance.statuses.last()
        if status is None:
            status = TransactionStatus.objects.create(
                transaction_status_type=TransactionStatusType.objects.get(
                    name="Error"),
                transaction=instance
            )
        return {
            "status": {
                "transactionStatus": status.transaction_status_type.name,
                "date": status.date,
                "merchantResponse": status.merchant_response
            },
            "id": instance.id,
            "salesOrderNumber": instance.id,
            "orderId": instance.order_id,
            "amount": str(instance.amount),
            "refId": instance.ref_id,
            "refTag": instance.ref_tag,
            "date": instance.date,
            "avsResponse": instance.avs_response,
            "avsDescription": instance.avs_description,
            "cardNumber": instance.card_number,
            "cardHolder": instance.card_holder.get_serializable_camel_case() if instance.card_holder else None
        }


class TransactionSimplerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'


class TransactionReportSerializer(serializers.ModelSerializer):
    merchant_account = MerchantSimpleSerializer()

    class Meta:
        model = Transaction
        fields = '__all__'
