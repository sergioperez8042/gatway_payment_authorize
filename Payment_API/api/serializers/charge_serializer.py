import logging

from rest_framework import serializers
from rest_framework.exceptions import APIException
from api.models import MerchantAccount
from api.background_process import background_process
from api.credit_card import CreditCard

from api.merchant_factory import MerchantFactory
from api.models import Application, CardHolder, Transaction, TransactionStatus, TransactionStatusType, TransactionType


class ChargeSerializer(serializers.Serializer):
    site_id = serializers.CharField()
    merchant_account_id = serializers.PrimaryKeyRelatedField(
        queryset=MerchantAccount.objects.all(), required=False)
    order_id = serializers.IntegerField()
    currency = serializers.CharField(max_length=3)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)

    card_number = serializers.CharField()
    cvv = serializers.CharField()
    expiry = serializers.CharField()
    card_type = serializers.CharField()

    card_name = serializers.CharField(max_length=250)
    address = serializers.CharField(max_length=250, required=False)
    city = serializers.CharField(max_length=250, required=False)
    zip_code = serializers.CharField(max_length=250, required=False)
    state = serializers.CharField(max_length=250, required=False)
    country = serializers.CharField(max_length=250, required=False)

    def __init__(self, **kwargs):
        self.logger = logging.getLogger(__name__)
        super().__init__(**kwargs)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        if not validated_data.get("merchant_account_id").is_active:
            raise APIException(
                detail='Merchant account not active', code='merchant_active')
        merchant = MerchantFactory().get_merchant(
            validated_data.get("merchant_account_id").merchant.name
        )
        transaction = Transaction()
        transaction.application_id = validated_data.get("site_id")
        transaction.order_id = validated_data.get("order_id")
        transaction.merchant_account = validated_data.get(
            "merchant_account_id")
        transaction.amount = validated_data.get("amount")
        transaction.set_masked_card(validated_data.get("card_number"))

        credit_card = CreditCard(
            card_name=validated_data.get("card_name"),
            card_number=validated_data.get("card_number"),
            card_type=validated_data.get("card_type"),
            expiry=validated_data.get("expiry"),
            cvv=validated_data.get("cvv"),
            currency=validated_data.get("currency"),
        )

        transaction.transaction_type = TransactionType.objects.get(
            name='Charge')

        card_holder = CardHolder()
        card_holder.card_name = validated_data.get("card_name")
        card_holder.address = validated_data.get("address")
        card_holder.city = validated_data.get("city")
        card_holder.zip_code = validated_data.get("zip_code")
        card_holder.state = validated_data.get("state")
        card_holder.country = validated_data.get("country")
        card_holder.save()

        transaction.card_holder = card_holder
        transaction.save()

        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Created'),
            transaction_type=transaction.transaction_type
        )
        transaction_status.save()

        return transaction

    def validate(self, data):
        # Validate merchant active
        merchant_account = data["merchant_account_id"]
        if not merchant_account.is_active:
            raise APIException(
                detail='Merchant account not active', code='merchant_active')

        # Validate currencies
        currencies = merchant_account.currencies.all()
        if len([item for item in currencies if item.code == str(data['currency']).upper()]) == 0:
            raise APIException(
                detail="Currency not valid for merchant", code="currency_invalid")

        # Validate card types
        card_types = merchant_account.card_types.all()
        if len([item for item in card_types if item.type == str(data['card_type']).upper()]) == 0:
            raise APIException(
                detail="Credit card type not supported.", code="card_type_error")

        # Validate merchant transaction limit
        if data['amount'] > merchant_account.transaction_limit:
            raise APIException(
                detail="Transaction is over limit.", code="transaction_limit")
        # Validate merchant daily limit
        if data['amount'] > merchant_account.get_daily_balance():
            raise APIException(
                detail="Transaction is over limit.", code="transaction_limit")
        # Validate merchant limit
        if data['amount'] > merchant_account.get_balance():
            raise APIException(
                detail="Transaction is over limit.", code="transaction_limit")

        if Transaction.objects.filter(
                application__id=data.get("site_id"),
                order_id=data.get("order_id"),
                statuses__transaction_status_type__name="Completed"
        ).count() > 0:
            raise APIException(
                detail='Duplicated transaction attempt.', code='duplicated_trans')
        return data

    def validate_site_id(self, data):
        try:
            return Application.objects.get(site_id=data).id
        except Application.DoesNotExist as ex:
            self.logger.exception(str(ex))
            raise APIException(
                detail="Application does not exists", code='site_missing')
