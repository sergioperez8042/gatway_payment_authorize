
import logging


from rest_framework import serializers
from api.models import TransactionStatus, TransactionStatusType
from api import background_process
from api.models import CardHolder
from api.credit_card import CreditCard
from api.merchant_factory import MerchantFactory
from api.models import Transaction, TransactionType
from rest_framework.exceptions import APIException

from api.models import MerchantAccount


class AuthorizeSerializer(serializers.Serializer):
    site_id = serializers.CharField()
    merchant_account_id = serializers.PrimaryKeyRelatedField(
        queryset=MerchantAccount.objects.all())
    order_id = serializers.IntegerField()
    currency = serializers.CharField(max_length=3)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)

    card_number = serializers.CharField()
    cvv = serializers.CharField()
    expiry = serializers.CharField()
    card_type = serializers.CharField()

    card_name = serializers.CharField(max_length=250)
    address = serializers.CharField(max_length=250)
    city = serializers.CharField(max_length=250)
    zip_code = serializers.CharField(max_length=250)
    state = serializers.CharField(max_length=250)
    country = serializers.CharField(max_length=250)
    phone = serializers.CharField(
        max_length=250, required=False, allow_null=True, allow_blank=True)
    email = serializers.EmailField(
        required=False, allow_null=True, allow_blank=True)
    ip = serializers.IPAddressField(
        required=False, allow_null=True, allow_blank=True)

    def __init__(self, **kwargs):
        self.transaction_exists = False
        self.transaction_status = None
        self.logger = logging.getLogger(__name__)
        super().__init__(**kwargs)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        merchant = MerchantFactory().get_merchant(
            validated_data.get("merchant_account_id").merchant.name)

        credit_card = CreditCard(
            card_name=validated_data.get("card_name"),
            card_type=validated_data.get("card_type"),
            card_number=str(validated_data.get(
                "card_number")).replace(" ", ""),
            expiry=str(validated_data.get("expiry")).replace(" ", ""),
            cvv=str(validated_data.get("cvv")).replace(" ", ""),
            currency=validated_data.get("currency"),
        )

        card_holder = CardHolder()
        card_holder.card_name = validated_data.get("card_name")
        card_holder.address = validated_data.get("address")
        card_holder.city = validated_data.get("city")
        card_holder.zip_code = validated_data.get("zip_code")
        card_holder.state = validated_data.get("state")
        card_holder.country = validated_data.get("country")
        card_holder.phone = validated_data.get("phone")
        card_holder.email = validated_data.get("email")
        card_holder.ip = validated_data.get("ip")
        card_holder.save()

        transaction = Transaction()
        transaction.application_id = validated_data.get("site_id")
        transaction.order_id = validated_data.get("order_id")

        transaction.merchant_account = validated_data.get(
            "merchant_account_id")
        transaction.amount = validated_data.get("amount")
        transaction.set_masked_card(validated_data.get("card_number"))

        transaction.transaction_type = TransactionType.objects.get(
            name='Authorize')

        transaction.card_holder = card_holder
        transaction.save()
        if self.transaction_status is None:
            self.transaction_status = TransactionStatus(
                transaction_id=transaction.id,
                transaction_status_type=TransactionStatusType.objects.get(
                    name='Created'),
                transaction_type=transaction.transaction_type
            )
            self.transaction_status.save()

        return transaction

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'siteId': instance.site_id,
            'orderId': instance.order_id,
            'amount': str(instance.amount),
            'cvv': instance.cvv,
            'merchantAccountId': instance.merchant_account.id,
            'cardNumber': instance.card_number,
            'date': instance.date,
            'status': instance.get_status()
        }
