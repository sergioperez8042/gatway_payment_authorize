

from rest_framework import serializers
from rest_framework.exceptions import APIException
from api.models import MerchantAccount
from api.background_process import background_process
from api.merchant_factory import MerchantFactory

from api.models import Transaction, TransactionStatus, TransactionStatusType, TransactionType


class CaptureSerializer(serializers.Serializer):
    transaction_id = serializers.IntegerField()
    site_id = serializers.CharField()
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        try:
            transaction = Transaction.objects.get(
                application_id=validated_data.get("site_id"),
                id=validated_data.get("transaction_id")
            )

        except Transaction.DoesNotExist:
            raise APIException(detail="Not found.", code="order_not_found")

        if validated_data.get("amount") > transaction.amount:
            raise APIException(detail="Amount not valid", code="not_valid")

        merchant = MerchantFactory().get_merchant(
            transaction.merchant_account.merchant.name
        )
        # Revisar!!!!
        if transaction.transaction_type.name == 'Authorize' and transaction.get_status().name == "Completed":
            transaction.transaction_type = TransactionType.objects.get(
                name='Capture')
            transaction.amount = validated_data.get("amount")
        transaction.save()

        transaction_status = TransactionStatus(
            transaction_id=transaction.id,
            transaction_status_type=TransactionStatusType.objects.get(
                name='Created'),
            transaction_type=transaction.transaction_type
        )
        transaction_status.save()

        return transaction
        raise APIException(
            detail='Transaction has to be authorized.', code='not_auth_trans')
