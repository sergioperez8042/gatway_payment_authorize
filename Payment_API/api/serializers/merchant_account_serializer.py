from rest_framework import serializers
from api.models import MerchantAccount
from api.serializers.credit_card_serializer import CardTypeSerializer
from api.serializers.currency_serializer import CurrencySerializer

from api.serializers.merchant_serializer import MerchantSerializer


class MerchantAccountForAdminSerializer(serializers.ModelSerializer):
    merchant = MerchantSerializer(required=False)
    card_types = CardTypeSerializer(many=True, read_only=True)
    currencies = CurrencySerializer(many=True, read_only=True)
    balance = serializers.SerializerMethodField()
    dailyBalance = serializers.SerializerMethodField()

    def get_balance(self, obj):
        return obj.get_balance()

    def get_dailyBalance(self, obj):
        return obj.get_daily_balance()

    class Meta:
        model = MerchantAccount
        fields = (
            'id',
            'name',
            'username',
            'merchantId',
            'password',
            'token_key',
            'is_active',
            'limit',
            'daily_limit',
            'transaction_limit',
            'dailyBalance',
            'balance',
            'merchant_timezone',
            'merchant',
            'card_types',
            'currencies'
        )


class MerchantAccountSerializer(serializers.ModelSerializer):
    merchant = MerchantSerializer()
    card_types = CardTypeSerializer(many=True,)
    currencies = CurrencySerializer(many=True)

    class Meta:
        model = MerchantAccount
        fields = (
            'id',
            'name',
            'is_active',
            'limit',
            'daily_limit',
            'transaction_limit',
            'merchant_timezone',
            'merchant',
            'card_types',
            'currencies',
            'merchant_company'
        )

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'transactionLimit': instance.transaction_limit,
            'dailyLimit': instance.daily_limit,
            'limit': instance.limit,
            'dailyBalance': instance.get_daily_balance(),
            'balance': instance.get_balance(),
            'merchantTimezone': instance.merchant_timezone,
            'isActive': instance.is_active,
            'merchant': {
                'id': instance.merchant.id,
                'name': instance.merchant.name
            },
            'cardTypes': [{'type': card.type} for card in instance.card_types.all()],
            'currencies': [{'type': currency.code} for currency in instance.currencies.all()],
            'merchantCompany': instance.merchant_company
        }
