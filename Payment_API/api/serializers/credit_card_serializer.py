from rest_framework import serializers

from api.models import CardType


class CardTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardType
        fields = ('type',)
