from rest_framework import serializers

from api.models import Merchant


class MerchantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Merchant
        fields = ('id', 'name')


class MerchantSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Merchant
        fields = ('id', 'name')
