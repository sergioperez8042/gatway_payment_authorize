from rest_framework import serializers

from api.models import TransactionStatus


class StatusSerializer(serializers.ModelSerializer):
    transaction_status = serializers.SerializerMethodField()

    def get_transaction_status(self, obj):
        if isinstance(obj, TransactionStatus):
            return obj.transaction_status_type.name
        transaction_status = TransactionStatus.objects.get(id=obj.get("id"))
        return transaction_status.transaction_status_type.name

    class Meta:
        model = TransactionStatus
        fields = ('transaction_status', 'date')
