from django.utils import timezone
from rest_framework.exceptions import APIException


class CreditCard:
    cards = [
        {
            'type': 'maestro',
            'patterns': [5018, 502, 503, 506, 56, 58, 639, 6220, 67],
            'length': [12, 13, 14, 15, 16, 17, 18, 19],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'visa',
            'patterns': [4],
            'length': [13, 16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'mastercard',
            'patterns': [51, 52, 53, 54, 55, 22, 23, 24, 25, 26, 27],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'american express',
            'patterns': [34, 37],
            'length': [15],
            'cvvLength': [3, 4],
            'luhn': True
        }, {
            'type': 'dinersclub',
            'patterns': [30, 36, 38, 39],
            'length': [14],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'discover',
            'patterns': [60, 64, 65, 622],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'unionpay',
            'patterns': [62, 88],
            'length': [16, 17, 18, 19],
            'cvvLength': [3],
            'luhn': False
        }, {
            'type': 'jcb',
            'patterns': [35],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }
    ]

    def __init__(self, card_name=None, card_number=None, card_type=None, expiry=None, cvv=None, currency=None):
        self.card_name = card_name
        self.card_number = card_number
        self.card_type = str(card_type).lower()
        self.card_expiry = expiry
        self.card_cvv = cvv
        self.currency = currency
        self.card_pattern = None

    def card_from_number(self, num):
        # find this card, based on the card number, in the defined set of cards
        for card in self.cards:
            for pattern in card['patterns']:
                if str(pattern) == str(num)[:len(str(pattern))]:
                    return card

    def validate_mod10(self, num):
        # validate card number using the Luhn (mod 10) algorithm
        checksum, factor = 0, 1
        for c in reversed(num):
            for c in str(factor * int(c)):
                checksum += int(c)
            factor = 3 - factor
        return checksum % 10 == 0

    def cvv_length_validation(self):
        for cvv_length in self.card_pattern["cvvLength"]:
            if cvv_length == len(self.card_cvv):
                return True
        return False

    def check_expiry(self):
        date = timezone.now()
        if int(self.card_expiry[2:4]) > int(date.year % 100):
            return True
        elif int(self.card_expiry[2:4]) == int(date.year % 100):
            if int(self.card_expiry[:2]) >= int(date.month / 100):
                return True
        return False

    def is_valid(self):
        # get the card type and its specs
        self.card_pattern = self.card_from_number(self.card_number)

        # if no card found, invalid
        if not self.card_pattern:
            raise APIException(detail='Credit card type not supported.', code='card_type_error')

        if self.card_type != self.card_pattern['type']:
            raise APIException(detail='Credit card pattern mismatch.', code='card_pattern_error')

        # check the length
        if not len(self.card_number) in self.card_pattern['length']:
            raise APIException(detail='Credit card length is not valid.', code='card_length_error')

        # test luhn if necessary
        if self.card_pattern['luhn']:
            if not self.validate_mod10(self.card_number):
                raise APIException(detail='Credit card number is not valid.', code='card_number_error')

        # check cvv length
        if not self.cvv_length_validation():
            raise APIException(detail='Credit card cvv is not valid.', code='card_cvv_error')

        # check expiry
        if not self.check_expiry():
            raise APIException(detail='Credit card expiration not valid.', code='card_expiration_error')

        return True
