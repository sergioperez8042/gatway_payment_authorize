from rest_framework.decorators import action
from rest_framework.response import Response
from api.models import MerchantAccount
from api.serializers.merchant_account_serializer import MerchantAccountSerializer
from api.serializers.capture_serializer import CaptureSerializer
from api.serializers.charge_serializer import ChargeSerializer
from api.serializers.transaction_serializer import TransactionSerializer
from api.models import Transaction
from api.serializers.authorize_serializer import AuthorizeSerializer
from decimal import*
from rest_framework import viewsets, status
from rest_framework.exceptions import APIException


# Create your views here.
class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    def list(self, request, *args, **kwargs):
        return super().list(request, args, kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, args, kwargs)

    @action(detail=False, methods=['post', 'get'])
    def authorize(self, request):
        data = request.data.copy()

        serializer = AuthorizeSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            transaction = serializer.create(serializer.validated_data)
            return Response(transaction.get_serializable_v1(), status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['put'])
    def capture(self, request):
        data = request.data.copy()
        serializer = CaptureSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            transaction = serializer.create(serializer.validated_data)
            return Response(transaction.get_serializable_v1(), status=status.HTTP_201_CREATED)
        raise APIException(detail='Unknown server error', code='server_error')

    @action(detail=False, methods=['post'])
    def charge(self, request):
        data = request.data.copy()
        serializer = ChargeSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            transaction = serializer.create(serializer.validated_data)
            return Response(transaction.get_serializable_v1(), status=status.HTTP_201_CREATED)
        raise APIException(detail='Unknown server error', code='server_error')


class MerchantViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MerchantAccount.objects.all()
    serializer_class = MerchantAccountSerializer

    def get_serializer_class(self):
        return MerchantAccountSerializer

    def list(self, request, *args, **kwargs):

        return super().list(request, args, kwargs)

    def retrieve(self, request, *args, **kwargs):

        return super().retrieve(request, args, kwargs)
