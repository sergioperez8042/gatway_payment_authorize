from api.merchant_interface import IAbstractMerchant


class MerchantFactory:
    def __init__(self):
        self.merchant = None
        self.merchant_dict = {
            'Authorize': authorize(),

        }

    def get_merchant(self, merchant_name: str) -> IAbstractMerchant:
        self.merchant = self.merchant_dict.get(merchant_name)
        return self.merchant


def authorize() -> IAbstractMerchant:
    authorize = IAbstractMerchant()
    from api.behaviors.authorize_behavior import authorize_behavior
    from api.behaviors.capture_behavior import capture_behavior
    from api.behaviors.charge_behavior import charge_behavior

    authorize.set_authorize_behavior(authorize_behavior)
    authorize.set_capture_behavior(capture_behavior)
    authorize.set_charge_behavior(charge_behavior)
    return authorize
