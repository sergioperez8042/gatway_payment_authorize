from datetime import timedelta, datetime
from django.utils import timezone
from django.conf import settings
from django.db import models
import pytz
from decimal import Decimal
from rest_framework.decorators import action
# Create your models here.

REFUND = 4


class MerchantType(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(
        max_length=250, default="no description available")

    def __str__(self):
        return self.name


class Merchant(models.Model):
    name = models.CharField(max_length=250)
    url = models.CharField(max_length=250)
    merchant_type = models.ForeignKey(
        MerchantType, on_delete=models.SET_NULL, null=True, db_index=True)

    def __str__(self):
        return self.name


class MerchantAccount(models.Model):
    id = models.IntegerField(primary_key=True)
    timezone_choices = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    name = models.CharField(max_length=250)
    username = models.CharField(max_length=250)
    merchantId = models.CharField(
        max_length=250, null=True, blank=True, default="")
    verify_url = models.CharField(max_length=255, null=True, blank=True)
    ok_url = models.CharField(max_length=255, null=True, blank=True)
    ko_url = models.CharField(max_length=255, null=True, blank=True)
    middle_page_url = models.URLField(null=True, blank=True)
    website_redirect_url_ko = models.URLField(null=True, blank=True)
    website_redirect_url_ok = models.URLField(null=True, blank=True)
    password = models.CharField(max_length=250)
    token_key = models.CharField(max_length=250, blank=True)
    merchant = models.ForeignKey(
        Merchant, on_delete=models.CASCADE, db_index=True)
    is_active = models.BooleanField()
    private_key = models.TextField(null=True, blank=True)
    retries = models.IntegerField(default=3, null=True, blank=True)
    expiration = models.DurationField(
        default=timedelta(minutes=30), null=True, blank=True)
    limit = models.DecimalField(max_digits=12, decimal_places=2,
                                help_text='Internal monthly assignation', default=0)

    daily_limit = models.DecimalField(default=1000, max_digits=12, decimal_places=2,
                                      help_text='Internal daily assignation')

    merchant_limit = models.DecimalField(max_digits=12, decimal_places=2,
                                         help_text='Internal monthly assignation')

    merchant_daily_limit = models.DecimalField(default=1000, max_digits=12, decimal_places=2,
                                               help_text='Internal daily assignation')
    transaction_limit = models.DecimalField(
        default=1000, max_digits=12, decimal_places=2,
        help_text='Maximum transaction amount')
    start_date_for_limit = models.DateTimeField(
        help_text='From this date on, transactions are going to be taken into account to check the limit')
    period_for_limit = models.IntegerField(
        help_text='Number of days before updating the start date for limit')
    merchant_timezone = models.CharField(
        max_length=100, choices=timezone_choices, default="UTC")
    merchant_company = models.CharField(max_length=255, blank=True, null=True)

    def check_limit(self, amount) -> bool:
        if amount > 0:
            return (self.get_balance() - Decimal(amount)) > 0
        return False

    def check_daily_limit(self, amount) -> bool:
        if amount > 0:
            return (self.get_daily_balance() - Decimal(amount)) > 0
        return False

    def check_transaction_limit(self, amount) -> bool:
        if amount > 0:
            return (self.transaction_limit - Decimal(amount)) > 0
        return False

    def get_balance(self):
        transactions = self.transaction_set.filter(date__gt=self.get_first_month_day()).filter(
            models.Q(transaction_type__name='Capture') |
            models.Q(transaction_type__name='Charge') |
            models.Q(transaction_type__name='Authorize')
        ).filter(
            models.Q(statuses__transaction_status_type__name__exact="Completed")
        ).distinct()

        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return self.limit - total_amount

    def get_amount(self):
        transactions = self.transaction_set.filter(date__gt=self.get_first_month_day()).filter(
            models.Q(transaction_type__name='Capture') |
            models.Q(transaction_type__name='Charge') |
            models.Q(transaction_type__name='Authorize')
        ).filter(
            models.Q(statuses__transaction_status_type__name__exact="Completed")
        ).distinct()

        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return total_amount

    def get_daily_balance(self):
        transactions = self.transaction_set.filter(date__gt=self.get_beginning_of_day()).filter(
            models.Q(transaction_type__name='Capture') |
            models.Q(transaction_type__name='Charge') |
            models.Q(transaction_type__name='Authorize')
        ).filter(
            models.Q(statuses__transaction_status_type__name__exact="Completed")
        ).distinct()

        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return self.daily_limit - total_amount

    def get_daily_amount(self):
        transactions = self.transaction_set.filter(date__gt=self.get_beginning_of_day()).filter(
            models.Q(transaction_type__name='Capture') |
            models.Q(transaction_type__name='Charge') |
            models.Q(transaction_type__name='Authorize')
        ).filter(
            models.Q(statuses__transaction_status_type__name__exact="Completed")
        ).distinct()

        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return total_amount

    def get_amount_processed(self):
        transactions = self.transaction_set.filter(date__gt=self.get_first_month_day()).filter(
            models.Q(transaction_type__name='Capture') | models.Q(transaction_type__name='Charge'))
        transactions = [x for x in transactions if
                        x.get_status().name == 'Captured' or x.get_status().name == 'Charged']
        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return total_amount

    def get_daily_amount_processed(self):
        transactions = self.transaction_set.filter(date__gt=self.get_beginning_of_day()).filter(
            models.Q(transaction_type__name='Capture') | models.Q(transaction_type__name='Charge'))
        transactions = [x for x in transactions if
                        x.get_status().name == 'Captured' or x.get_status().name == 'Charged']
        total_amount = 0
        for transaction in transactions:
            total_amount = total_amount + transaction.amount
        return total_amount

    def get_first_month_day(self):
        return datetime.replace(timezone.now().astimezone(pytz.timezone(self.merchant_timezone)), day=1, hour=0,
                                minute=0,
                                second=0, microsecond=0)

    def get_beginning_of_day(self):
        return datetime.replace(timezone.now().astimezone(pytz.timezone(self.merchant_timezone)), hour=0,
                                minute=0, second=0, microsecond=0)

    def update_limit_date(self):
        now = timezone.now().astimezone(pytz.timezone(self.merchant_timezone))
        if self.start_date_for_limit + timedelta(days=self.period_for_limit) < now:
            self.start_date_for_limit = now


class CardType(models.Model):
    type_choices = (
        ('VISA', 'VISA'),
        ('MASTERCARD', 'MASTERCARD'),

    )
    type = models.CharField(max_length=50, choices=type_choices)
    merchant_account = models.ForeignKey(MerchantAccount, on_delete=models.CASCADE, null=True, blank=True,
                                         db_index=True, related_name='card_types')

    def __str__(self):
        return self.type


class Currency(models.Model):
    code_choices = (('USD', 'USD'), ('EUR', 'EUR'))
    code = models.CharField(max_length=3, choices=code_choices)
    merchant_account = models.ForeignKey(MerchantAccount, on_delete=models.CASCADE, null=True, blank=True,
                                         db_index=True, related_name='currencies')

    def __str__(self):
        return self.code


class Application(models.Model):
    name = models.CharField(max_length=250)
    application_url = models.CharField(max_length=250)
    site_id = models.CharField(max_length=20, unique=True)
    merchant_accounts = models.ManyToManyField(MerchantAccount, related_name='applications', blank=True,
                                               db_index=True)

    def __str__(self):
        return self.name


class TransactionType(models.Model):
    transaction_type_choices = (
        ("Authorize", "Authorize"),
        ("Capture", "Capture"),
        ("Charge", "Charge"),

    )
    name = models.CharField(max_length=250, choices=transaction_type_choices)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class CardHolder(models.Model):
    card_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250, null=True)
    city = models.CharField(max_length=250, null=True)
    zip_code = models.CharField(max_length=250, null=True)
    state = models.CharField(max_length=250, null=True)
    country = models.CharField(max_length=250, null=True)
    phone = models.CharField(max_length=250, null=True, default="")
    email = models.EmailField(null=True, db_index=True)
    consumer_id = models.CharField(max_length=250, null=True)
    document_id = models.CharField(
        max_length=20, null=True, blank=True, default="")
    ip = models.GenericIPAddressField(null=True, blank=True)

    def get_serializable(self):
        return {
            "card_name": self.card_name,
            "address": self.address,
            "city": self.city,
            "zip_code": self.zip_code,
            "state": self.state,
            "country": self.country,
            "phone": self.phone,
            "email": self.email
        }

    def get_serializable_camel_case(self):
        return {
            "cardName": self.card_name,
            "address": self.address,
            "city": self.city,
            "zipCode": self.zip_code,
            "state": self.state,
            "country": self.country,
            "phone": self.phone,
            "email": self.email
        }

    def __str__(self):
        return self.card_name


@action(detail=False, name='api_transaction')
class Transaction(models.Model):
    application = models.ForeignKey(Application, on_delete=models.SET_NULL, null=True, related_name="transactions",
                                    db_index=True)
    order_id = models.IntegerField()
    merchant_account = models.ForeignKey(
        MerchantAccount, on_delete=models.SET_NULL, null=True, db_index=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    ref_id = models.CharField(max_length=250)
    ref_tag = models.CharField(max_length=250, default="")
    date = models.DateTimeField(auto_now_add=True)
    card_number = models.CharField(max_length=250, default="")
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.SET_NULL, null=True, blank=True,
                                         db_index=True)
    card_holder = models.OneToOneField(
        to=CardHolder, on_delete=models.SET_NULL, null=True, db_index=True, blank=True)
    avs_response = models.CharField(max_length=250, default='')
    avs_description = models.TextField(
        default='AVS not available for this transaction.')

    ok_redirect = models.URLField(null=True, blank=True)
    ko_redirect = models.URLField(null=True, blank=True)
    merchant_form_visited = models.BooleanField(default=False)
    retries = models.IntegerField(default=3, null=True, blank=True)

    def get_status(self):
        # if self.transaction_type.name == TransactionType.transaction_type_choices[REFUND][0]:
        #     return self.refunds.last().transaction_status_type
        return self.statuses.last().transaction_status_type if self.statuses.all().count() > 0 \
            else TransactionStatusType(name='Unknown', description='Old transaction')

    def get_status_v1(self):
        return self.statuses.all().order_by('-id')[0] if self.statuses.all().count() > 0 \
            else None

    def __str__(self):
        return str(self.id)

    def set_masked_card(self, card_number):
        self.card_number = card_number[:4] + '****' + card_number[-4:]

    def get_serializable(self):
        status = self.get_status_v1()
        date_time = status.date.astimezone(pytz.timezone(
            settings.TIME_ZONE)).strftime('%m/%d/%Y %H:%M')

        serializable = {
            'id': self.id,
            'merchant': self.merchant_account.name,
            'application': self.application.name,
            'order_id': self.order_id,
            'amount': str(self.amount),
            'date': date_time,
            'status': str(status.transaction_status_type) if status is not None else str(
                TransactionStatusType(name='Unknown', description='Old transaction'))
        }
        return serializable

    def get_serializable_with_no_status(self):
        serializable = {
            'id': self.id,
            'merchant': self.merchant_account.name,
            'application': self.application.name,
            'order_id': self.order_id,
            'amount': str(self.amount)
        }
        return serializable

    def get_serializable_merchant_timezone(self):
        status = self.get_status_v1()
        date_time = status.date.astimezone(pytz.timezone(self.merchant_account.merchant_timezone)).strftime(
            '%m/%d/%Y %H:%M')

        serializable = {
            'id': self.id,
            'merchant': self.merchant_account.name,
            'application': self.application.name,
            'order_id': self.order_id,
            'amount': str(self.amount),
            'date': date_time,
            'status': str(status.transaction_status_type) if status is not None else str(
                TransactionStatusType(name='Unknown', description='Old transaction'))
        }
        return serializable

    def get_serializable_v1(self):
        date_time = self.date.astimezone(pytz.timezone(
            settings.TIME_ZONE)).strftime('%m/%d/%Y %H:%M')
        serializable = {
            'id': self.id,
            'order_id': self.order_id,
            'amount': str(self.amount),
            'merchant_account_id': self.merchant_account.id,
            'card_number': self.card_number,
            'date': date_time,
        }
        return serializable

    def get_serializable_merchant_timezone_v1(self):
        date_time = self.date.astimezone(pytz.timezone(self.merchant_account.merchant_timezone)).strftime(
            '%m/%d/%Y %H:%M')
        serializable = {
            'id': self.id,
            'order_id': self.order_id,
            'amount': str(self.amount),
            'merchant_account_id': self.merchant_account.id,
            'card_number': self.card_number,
            'date': date_time,
            'status': str(self.get_status())
        }
        return serializable


class TransactionStatusType(models.Model):
    status_type_choices = (
        ("Created", "Created"),
        ("Completed", "Completed"),
        ("Error", "Error"),
    )
    name = models.CharField(
        max_length=25, choices=status_type_choices, db_index=True)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class TransactionStatus(models.Model):
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.SET_NULL, null=True, blank=True,
                                         db_index=True)
    transaction_status_type = models.ForeignKey(TransactionStatusType, on_delete=models.SET_NULL, blank=True, null=True,
                                                db_index=True)
    date = models.DateTimeField(auto_now_add=True)
    transaction = models.ForeignKey(Transaction, on_delete=models.SET_NULL, blank=True, null=True,
                                    related_name='statuses', db_index=True)
    merchant_response = models.TextField(default="N/A")

    def __str__(self):
        return str(self.transaction_status_type) + " - " + str(self.date)


class TransactionLog(models.Model):
    transaction = models.ForeignKey(
        Transaction, on_delete=models.PROTECT, related_name="transactions")
    date = models.DateTimeField(auto_now_add=True)
    log = models.TextField()

    def __str__(self):
        return str(self.transaction.id) + ' - ' + str(self.date)
